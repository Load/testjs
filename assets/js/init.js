$(function () {
	$('.js-get-data').on('click', function () {
		alertify.log('Ładowanie...');

		$.getJSON('result.json', function (result) {
			if (!result.success) {
				alertify.error(result.message);
			} else {
				alertify.success('Załadowano poprawnie');

				$('.js-get-data').text('Pobierz ponownie');

				var template = Handlebars.compile($('#template-data').html());

				$('.js-data').html(template(result));

				$('.js-table').DataTable({
					paging: false,
					info: false,
					searching: false
				});
			}
		});

		return false;
	});
});